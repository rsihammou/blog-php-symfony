export DEBIAN_FRONTEND=noninteractive
apt-get update -y -qq >/dev/null 2>&1
apt-get install -y curl wget unzip

# Install requirements for Symfony
echo "[2]- Install requirements for Symfony"
apt-get install -y php php-bcmath php-cli php-curl php-zip php-sqlite3 php-mysql php-xml php-mbstring
wget https://getcomposer.org/composer.phar
mv composer.phar /usr/bin/composer
chmod +x /usr/bin/composer

# Install Gitlab runner
echo "[3]- Install Gitlab runner"
export GITLAB_RUNNER_DISABLE_SKEL=true
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
sudo -E apt-get install gitlab-runner -y

# Register Gitlab runner
echo "[4]- Register Gitlab runner"
gitlab-runner register \
    --non-interactive \
    --executor "shell" \
    --url $1 \
    --registration-token $2 \
    --description $3 \
    --tag-list $4

gitlab-runner start
