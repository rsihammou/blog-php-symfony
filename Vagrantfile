# -*- mode: ruby -*-
# vi: set ft=ruby :
 
Vagrant.configure("2") do |config|
  config.vm.box = "debian/buster64"
  config.vm.box_check_update = false
  config.vm.provider "virtualbox" do |vb|
    vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
    vb.customize ["modifyvm", :id, "--memory", 2048]
    vb.customize ["modifyvm", :id, "--cpus", "2"]
  end 
  config.vm.define "recette" do |recette|
    recette.vm.hostname = "recette"
    recette.vm.network "forwarded_port", guest: 8000, host: 8010
    recette.vm.network "private_network", ip: "192.168.34.10" 
    recette.vm.synced_folder ".", "/vagrant"
    recette.vm.provision :shell do |s|
      s.path = "init_instance.sh"
      s.args = "#{ENV['URL']} #{ENV['TOKEN']} staging-runner 'staging,test'"
    end
  end

  config.vm.define "production" do |production|
    production.vm.hostname = "production"
    production.vm.network "forwarded_port", guest: 8000, host: 8080
    production.vm.network "private_network", ip: "192.168.34.20" 
    production.vm.synced_folder ".", "/vagrant"
    production.vm.provision :shell do |s|
      s.path = "init_instance.sh"
      s.args = "#{ENV['URL']} #{ENV['TOKEN']} prod-runner 'prod'"
    end
  end

  config.vm.define "gitlab" do |gitlab|
    gitlab.vm.hostname = "gitlab"
    gitlab.vm.network "private_network", ip: "192.168.34.30"
    gitlab.vm.provider "virtualbox" do |vb|
      vb.customize ["modifyvm", :id, "--memory", 4096]
    end
    gitlab.vm.provision "shell", inline: <<-SHELL
        # Install gitlab
        echo "[2]: Install gitlab"
        apt-get update -qq >/dev/null
        apt-get install -qq -y vim git wget curl openssh-server ca-certificates >/dev/null
        curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash
        export LC_CTYPE=en_US.UTF-8
        export LC_ALL=en_US.UTF-8
        apt-get install -y gitlab-ce -qq >/dev/null
        gitlab-ctl reconfigure
    SHELL
  end
  config.vm.provision "shell", inline: <<-SHELL
      # Activate ssh connection for vagrant user
      echo "[1]- Activate ssh connection for vagrant user"
      sed -i 's/ChallengeResponseAuthentication no/ChallengeResponseAuthentication yes/g' /etc/ssh/sshd_config
      service ssh restart
  SHELL
end

