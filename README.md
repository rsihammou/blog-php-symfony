# Aperçu
Dans ce projet, et à l'aide de GitLab nous allons automatiser une chaîne de production classique.
La plupart des anciennes chaînes de production est basée sur des actions manuelles, ce qui induit souvent à des erreurs humaines.

![Chaîne de production classique](images/classic-deployment.png)

Dans ce schéma, les phases de livraisons en recette et en production sont réalisées manuellement via FTP, on se retrouve souvent dans les situations suivantes :
- Un temps considérable pour chercher et choisir les bons fichiers à envoyer via FTP. 
- On n'est pas à l'abri d'un oublie ou une erreur humaine.
- Des livraisons ratées à cause des incompatibilités non prévues.
- Le retour en arrière (version précédente) n'est pas trivial et rapide ce qui implique une perte de chiffre d'affaires non négligeable.
- Il n'est pas toujours facile de garantir une date de livraison fixe.

Pour remédier à ce problème, la meilleure façon est d'automatiser la chaîne d'intégration et de déploiement.
Ceci est possible par exemple via GitLab et GitLab-ci.

![Chaîne de production automatisée](images/gitlab-ci-cd.png)

Il suffit de publier les modifications vers le serveur GitLab pour déclencher le cycle d'intégation continue:
- Installation des dépendances.
- Exécution des tests unitaires.
- déploiement automatiquement de l'application en recette.
  
Une fois l'application est stable, les developpeurs peuvent tagger sa version, ainsi le déploiement en production devient possible en activant manuellement le process.


## Infrastructure

### Schéma globale

l'infrastructure est définie comme suivant: 

![Infrastructure](images/infrastructure.png)

- VM GitLab (`192.168.34.30`) qui hébergera le serveur GitLab.
- Vm recette (`192.168.34.10`) qui fera l'office de serveur de test.
- Vm production (`192.168.34.20`) qui fera l'office de serveur de production.

### Scripts de création

Nous utilisons Vagrant pour créer l'infrastructure :

[Vagrantfile](Vagrantfile)
```sh
# -*- mode: ruby -*-
# vi: set ft=ruby :
Vagrant.configure("2") do |config|
  config.vm.box = "debian/buster64"
  config.vm.box_check_update = false
  config.vm.provider "virtualbox" do |vb|
    vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
    vb.customize ["modifyvm", :id, "--memory", 2048]
    vb.customize ["modifyvm", :id, "--cpus", "2"]
  end 
  config.vm.define "recette" do |recette|
    recette.vm.hostname = "recette"
    recette.vm.network "forwarded_port", guest: 8000, host: 8010
    recette.vm.network "private_network", ip: "192.168.34.10" 
    recette.vm.synced_folder ".", "/vagrant"
    recette.vm.provision :shell do |s|
      s.path = "init_instance.sh"
      s.args = "#{ENV['URL']} #{ENV['TOKEN']} staging-runner 'staging,test'"
    end
  end

  config.vm.define "production" do |production|
    production.vm.hostname = "production"
    production.vm.network "forwarded_port", guest: 8000, host: 8080
    production.vm.network "private_network", ip: "192.168.34.20" 
    production.vm.synced_folder ".", "/vagrant"
    production.vm.provision :shell do |s|
      s.path = "init_instance.sh"
      s.args = "#{ENV['URL']} #{ENV['TOKEN']} prod-runner 'prod'"
    end
  end

  config.vm.define "gitlab" do |gitlab|
    gitlab.vm.hostname = "gitlab"
    gitlab.vm.network "private_network", ip: "192.168.34.30"
    gitlab.vm.provider "virtualbox" do |vb|
      vb.customize ["modifyvm", :id, "--memory", 4096]
    end
    gitlab.vm.provision "shell", inline: <<-SHELL
        # Install gitlab
        echo "[2]: Install gitlab"
        apt-get update -qq >/dev/null
        apt-get install -qq -y vim git wget curl openssh-server ca-certificates >/dev/null
        curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash
        export LC_CTYPE=en_US.UTF-8
        export LC_ALL=en_US.UTF-8
        apt-get install -y gitlab-ce -qq >/dev/null
        gitlab-ctl reconfigure
    SHELL
  end
  config.vm.provision "shell", inline: <<-SHELL
      # Activate ssh connection for vagrant user
      echo "[1]- Activate ssh connection for vagrant user"
      sed -i 's/ChallengeResponseAuthentication no/ChallengeResponseAuthentication yes/g' /etc/ssh/sshd_config
      service ssh restart
  SHELL
end
```
Script d'installation et configuration des VM `recette` et `production`.
```sh
export DEBIAN_FRONTEND=noninteractive
apt-get update -y -qq >/dev/null 2>&1
apt-get install -y curl wget unzip

# Install requirements for Symfony
echo "[2]- Install requirements for Symfony"
apt-get install -y php php-bcmath php-cli php-curl php-zip php-sqlite3 php-mysql php-xml php-mbstring
wget https://getcomposer.org/composer.phar
mv composer.phar /usr/bin/composer
chmod +x /usr/bin/composer

# Install Gitlab runner
echo "[3]- Install Gitlab runner"
export GITLAB_RUNNER_DISABLE_SKEL=true
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
sudo -E apt-get install gitlab-runner -y

# Register Gitlab runner
echo "[4]- Register Gitlab runner"
gitlab-runner register \
    --non-interactive \
    --executor "shell" \
    --url $1 \
    --registration-token $2 \
    --description $3 \
    --tag-list $4

gitlab-runner start
```
Ce script assure une :
- Installation des prérequis pour le bon fonctionnement de framework Symfony.
- Installation et configuration de runner GitLab.

### Procédure d'installation

#### VM GitLab

##### 1-Installation 

D'abord il faut commencer par installer le serveur GitLab :
```bash
$ git clone https://gitlab.com/rsihammou/blog-php-symfony.git
$ cd blog-php-symfony
$ vagrant up gitlab
```
Une fois l'installation terminée, ajouter la ligne suivante au fichier `/etc/hosts` :

```bash
$ sudo vim /etc/hosts

192.168.35.30   gitlab.example.com
```

Vous pouvez accéder au GitLab via l'url : [http://gitlab.example.com](http://gitlab.example.com)

La première fois on vous demandera de changer le mot de passe `root`.

![Première connexion à GitLab](images/first-launch-gitlab.png)

Connectez-vous ensuite avec l'utilisateur root et le mot de passe que vous venez de mettre en place.

![Connecter avec le compte root](images/root-sing-in.png)

##### 2-Ajouter un nouveau utilisateur

Pour continuer, il vaut mieux créer un utilisateur autre que le root, et ça sera facile en désactivant la probation par l'administrateur.  
Allez à la page : **Settings > General**  
Au niveau de la section **Sing-up restrictions** découchez l'option **Require admin approval for new sing-ups**

![Désactiver la probation administrateur](images/disable-admin-approval.png)

Maintenant, vous pouvez créer votre utilisateur :  
Allez à la page : **Admin area > Users**  

![Ajouter un nouveau utilisateur](images/create-user.png)

Déconnectez-vous et connectez-vous avec le nouveau utilisateur.  

##### 3-Ajouter la clef SSH

Pour configurer la communication SSH entre votre machine local et le serveur GitLab, 
vous devez ajouter votre clef publique comme suivant :  
Allez à la page **Settings > SSH Keys**

![SSH configuration](images/add-ssh-key.png)

##### 4-Créer un nouveau projet

Créez un nouveau projet, et donnez le un nom significatif.

![Créer un nouveau projet](images/create-project.png)

Suivez les instructions pour ajouter le projet `blog-php-symfony` importé au préalable sur votre machine.

![GitLab instructions](images/new-project-instructions.png)

#### VM recette

##### 1-Installation

Avant de lancer l'installation, il faut récupérer le TOKEN lié à votre projet pour enregistrer les runners GitLab.

Allez à la page : **Settings > CI/CD** et dérouler la section **Runners**

![Runners configuration](images/runner-configuration.png)

Lancez le script vagrant comme suivant : 
```bash
$ URL=https://gitlab.com/ TOKEN=######### vagrant up recette
```

#### VM production

##### 1-Installation

De la même manière, lancez la procédure d'installation :

Lancez le script vagrant comme suivant :

```bash
$ URL=https://gitlab.com/ TOKEN=######### vagrant up production
```

## GitLab-ci

Le fichier `.gitlab-ci.yml` contient la configuration de processus d'intégration et déploiement continue.

![Branche pipeline](images/branche-pipeline.png)
![Tag pipeline](images/tag-pipeline.png)

Le processus est composé de la liste des jobs suivantes :
- **test_job:** exécute les tests unitaires.
- **deploy_recette:** lance le déploiement de l'application en recette, ce job dépend de `test_job`.
- **deploy_prod:** lance le déploiement de l'application en prod, ce job dépend de `test_job` et nécessite une validation manuelle pour l'activer.

### test_job

```yaml
before_script:
  - composer install --optimize-autoloader --no-progress --no-interaction --no-ansi --no-scripts

test_job:
  stage: test
  script:
    - php bin/phpunit --coverage-text
  tags:
    - test
```
À chaque publication de modifications de code, le pipeline se déclenche automatiquement. 
On commence d'abord par installer les prérequis logiciels via `composer`, ensuite une phase de tests unitaires est déroulée.  
Le job porte le tag `test`, il sera lancé sur le runner installé sur la VM recette.

### deploy_recette
```yaml
before_script:
  - composer install --optimize-autoloader --no-progress --no-interaction --no-ansi --no-scripts

deploy_recette:
  stage: deploy
  needs:
    - test_job
  script:
    - php bin/console server:start 0.0.0.0:8000 -q > /dev/null 2>&1
  tags:
    - staging
  only:
    - master
```
Une fois le test_job est bien passé et sans aucune erreur, le runner GitLab exécute la phase de déploiement en recette.  
Le déploiement se déroule comme suivant :
- Installation des prérequis via composer.
- Démarre l'application sur le port 8000.

Le job porte le tag `staging`, il sera lancé sur le runner installé sur la VM recette.

### deploy_prod
```yaml
before_script:
  - composer install --optimize-autoloader --no-progress --no-interaction --no-ansi --no-scripts
  
deploy_prod:
  stage: deploy
  needs:
    - test_job
  script:
    - export SYMFONY_ENV=prod
    - php bin/console server:start 0.0.0.0:8000 -q > /dev/null 2>&1
  when: manual
  only:
    - tags
  tags:
    - prod
```

Ce job sera active une fois un tag de l'application est publié, une action manuelle est nécessaire pour lancer ce job.  
Le job porte le tag `prod`, il sera lancé sur le runner installé sur la VM production.  
Pour créer un tag de l'application, vous pouvez procéder comme suivant :
```bash
$ git tag -a v1.0 -m "Version 1.0"
$ git push origin v1.0
```

